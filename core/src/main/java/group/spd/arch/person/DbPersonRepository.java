package group.spd.arch.person;

import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class DbPersonRepository implements PersonRepository, PersonProvider {

    private final DataSource dataSource;

    public DbPersonRepository(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Optional<Person> getById(int id) {
        final String sql = "select * from persons where id = ?";

        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, id);

            try (final ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(toPerson(rs));
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public Person save(Person person) {
        final String sql = "insert into persons(name, email) VALUES (?, ?)";

        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, person.getName());
            ps.setString(2, person.getEmail());
            ps.executeUpdate();

            try (final ResultSet rs = ps.getGeneratedKeys()) {
                rs.next();
                person.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return person;
    }

    @Override
    public List<Person> getAll() {
        final List<Person> persons = new ArrayList<>();
        final String sql = "select * from persons";

        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement ps = connection.prepareStatement(sql);
             final ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                persons.add(toPerson(rs));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return persons;
    }

    private Person toPerson(ResultSet rs) throws SQLException {
        final Person person = new Person();
        person.setId(rs.getInt("id"));
        person.setName(rs.getString("name"));
        person.setEmail(rs.getString("email"));
        return person;
    }
}
