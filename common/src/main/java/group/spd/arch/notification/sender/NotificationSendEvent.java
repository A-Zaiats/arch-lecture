package group.spd.arch.notification.sender;

import java.io.Serializable;
import java.util.Map;

public class NotificationSendEvent implements Serializable {

    private final String notificationType;
    private final Map<String, String> params;
    private final String assigneeEmail;

    public NotificationSendEvent(String notificationType, Map<String, String> params, String assigneeEmail) {
        this.notificationType = notificationType;
        this.params = params;
        this.assigneeEmail = assigneeEmail;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public String getAssigneeEmail() {
        return assigneeEmail;
    }
}
